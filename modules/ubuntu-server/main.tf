terraform {
  required_version = ">= 1.0.0, < 2.0.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

data "aws_availability_zones" "available" {}

data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}



resource "aws_security_group" "web" {
  name = "${var.cluster_name}-security_group_service"
  dynamic "ingress" {
    for_each = ["22", "80", "8080", "9090", "9100", "3000", "9093", "8081", "9080", "3100", "8083", "443"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Web access for service instance-${var.cluster_name}"
  }
}

resource "aws_instance" "my_webserver" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_security_group.web.id]
  key_name = var.ssh_pub_key
  root_block_device {
    volume_size = 500
    volume_type = "gp3"
  }
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "Instahelper Server IP${var.cluster_name}"
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_eip" "my_static_ip" {
  instance = aws_instance.my_webserver.id
  tags = {
    Name  = "Server IP address"
  }
}
