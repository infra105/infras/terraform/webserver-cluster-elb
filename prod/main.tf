terraform {
  required_version = ">= 1.0.0, < 2.0.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

module "webserver-cluster" {
  source = "../modules/webserver-cluster"

  cluster_name = var.cluster_name 

  instance_type = var.instance_type
  min_size = var.min_size
  max_size = var.max_size
}

module "webserver-ubuntu" {
  source = "../modules/ubuntu-server"

  cluster_name = var.cluster_name

  instance_type = var.instance_type
  min_size = var.min_size
  max_size = var.max_size
}
