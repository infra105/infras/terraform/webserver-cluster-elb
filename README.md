# WebCluster AWS
Скопируйте себе репозиторий при помощи команды:
git clone https://gitlab.com/infra105/infras/terraform/webserver-cluster-elb.git

## terraform
Для начала, установите terraform

* https://learn.hashicorp.com/tutorials/terraform/install-cli

После чего нужно перейти в папку terraform и развернуть сервера при помощи команды:
- terraform init
- terraform apply --auto-approve


